# llvm-z80-toolchain

A toolchain for getting modern languages on the Z80 thanks to LLVM(-CBE) and SDCC

## Dependencies

just a quick writeup for now, I *will* replace this with submodules and a script of some kind.

### LLVM

just install LLVM version 12 (or above?).

I successfully tested:
```
$ clang --version      
clang version 12.0.1
Target: x86_64-unknown-linux-gnu
Thread model: posix
InstalledDir: /usr/bin
```

### LLVM-CBE

```
git clone https://github.com/JuliaComputing/llvm-cbe
cd llvm-cbe && mkdir build && cd build
cmake -S ..
make llvm-cbe
```

### SDCC

well, yeah, you'll need that too.

## Workflow example

assuming a `hello.cpp` input, and Z80 output (assembler only for now, since i haven't figured out how to link a `libc(++)` yet).

1. Compile source into LLVM IL:
   `clang++ -O3 -emit-llvm hello.cpp -S -o hello.ll`
	 (generates human-readable `hellp.ll` intermediate language)
2. Convert IL back into C:
   `llvm-cbe hello.ll`
	 (generates human-readable `hello.cbe.c`)
3. Compile C into assembler:
   `sdcc -mz80 -S --stack-auto hello.cbe.c -o hello.cbe.z80`
	 (generates human-readable `hello.cbe.z80`)

most likely you'll need to clean up the generated C code (`hello.cbe.c` from step 2) to get rid of things like `__attribute__ ((packed))` (which is SDCC default anyway), `__noreturn` or `builtin_*`. I'm working on a C header to shove into sdcc that takes care of this.
